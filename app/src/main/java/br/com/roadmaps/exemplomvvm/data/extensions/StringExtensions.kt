package br.com.roadmaps.exemplomvvm.data.extensions

fun String.replaceMultiple(vararg params: Pair<String, Any>): String{
    var result = this
    params.forEach { (t, u) ->
        result = result.replace(t, u.toString())
    }
    return result
}

fun String.setUrl(): String {
//    return  "${URL}/$this"
    return  ""
}

fun String.urlParams(vararg params: Pair<String, Any>):String {
    return this.setUrl().replaceMultiple(*params)
}