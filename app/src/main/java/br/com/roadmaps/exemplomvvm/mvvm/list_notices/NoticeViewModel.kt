package br.com.roadmaps.exemplomvvm.mvvm.list_notices

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.roadmaps.exemplomvvm.data.extensions.toJson
import br.com.roadmaps.exemplomvvm.data.model.Notice
import br.com.roadmaps.exemplomvvm.data.model.ResponseNetwork
import br.com.roadmaps.exemplomvvm.data.network.Network
import br.com.roadmaps.pparena.data.network.CallbackNetwork


class NoticeViewModel : ViewModel(){



    var loading = MutableLiveData<Boolean>()
    var loadingSwipe = MutableLiveData<Boolean>()
    var listNotice = MutableLiveData<List<Notice>>()
    var listNoticePaginate = MutableLiveData<List<Notice>>()
    private var pages = 0



    fun getListNotice(hasPaginate: Boolean){
        loading.postValue(true)
        Network().requestListNotice(object : CallbackNetwork {   // TODO: trocar o CallbackNetwork por MutableLiveData
            override fun onSuccess(response: ResponseNetwork?) {
                if (response?.code == 200) {
                    if (hasPaginate){
                        listNoticePaginate.postValue(response.message?.toJson())
                    }else {
                        listNotice.postValue(response.message?.toJson())
                    }
                    loading.postValue(false)
                    loadingSwipe.postValue(false)

                }
            }

            override fun onError(error: ResponseNetwork?) {
                loading.postValue(false)
                loadingSwipe.postValue(false)
            }
        })
    }




}