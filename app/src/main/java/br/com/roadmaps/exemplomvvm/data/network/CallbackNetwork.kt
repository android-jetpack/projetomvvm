package br.com.roadmaps.pparena.data.network

import br.com.roadmaps.exemplomvvm.data.model.ResponseNetwork


interface CallbackNetwork {
    fun onSuccess(response: ResponseNetwork?)
    fun onError(error: ResponseNetwork?)
}