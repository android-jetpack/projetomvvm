package br.com.roadmaps.exemplomvvm.mvvm.list_notices

import android.nfc.tech.MifareUltralight.PAGE_SIZE
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import br.com.roadmaps.exemplomvvm.R
import br.com.roadmaps.exemplomvvm.common.BaseActivity
import br.com.roadmaps.exemplomvvm.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*


class ListNoticesActivity : BaseActivity() {



    private val binding by binding<ActivityMainBinding>(R.layout.activity_main)
    // viewHolder com parametros
//    private val viewModel: NoticeViewModel by lazy { ViewModelProvider(this@ListNoticesActivity, ViewModelProviderFactory(NoticeRepository()))[NoticeViewModel::class.java] }
    // viewHolder sem parametros
    private val viewModel: NoticeViewModel by lazy { ViewModelProvider(this@ListNoticesActivity)[NoticeViewModel::class.java] }
    private var isLoading = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setup()
        events()
    }



    private fun setup(){
        binding.apply {
            lifecycleOwner = this@ListNoticesActivity
            viewModel = this@ListNoticesActivity.viewModel
            executePendingBindings()
        }

        viewModel.getListNotice(false)

        //Jeito 1
//        viewModel.loading.postValue(true)
//        viewModel.listNotice.observe(this, Observer{
//            viewModel.loading.postValue(false)
//        })


        //Jeito 2
//        viewModel.getListNotice().observe(this, Observer{
//            it.map { Log.i("TAG", it.title!!) }
//
//            binding.recyclerNotice.apply {
//                adapter = AdapterNotice(it)
//            }
//        })
    }



    private fun events(){
        /* Evento de paginação do recycleView */
        recyclerNotice.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount: Int = recyclerView.layoutManager?.childCount!!
                val totalItemCount: Int = recyclerView.layoutManager?.itemCount!!
                val firstVisibleItemPosition: Int = (recyclerView.layoutManager as LinearLayoutManager?)!!.findFirstVisibleItemPosition()
                if (isLoading) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                        viewModel.getListNotice(true)
                        isLoading = false
                    }
                }else{
                    isLoading = true
                }
            }
        })



        refreshLayout.setOnRefreshListener {
            viewModel.getListNotice(false)
        }
    }
}