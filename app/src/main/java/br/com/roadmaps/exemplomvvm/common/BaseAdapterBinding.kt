package br.com.roadmaps.exemplomvvm.common

import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import br.com.roadmaps.exemplomvvm.R
import br.com.roadmaps.exemplomvvm.data.model.Notice
import br.com.roadmaps.exemplomvvm.mvvm.adapters.AdapterNotice
import br.com.roadmaps.exemplomvvm.mvvm.list_notices.NoticeViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class BaseAdapterBinding {

    companion object {

        var adapter: AdapterNotice? = null

        /*
         vai setar a imagem no glide do adapter, vai linkado no .xml do adapter
        */
        @JvmStatic
        @BindingAdapter("app:image_url")
        fun setImageUrl(imgView: ImageView, imgUrl: String?) {
            imgUrl?.let {
                Glide.with(imgView.context).load(imgUrl).apply(
                    RequestOptions()
                        .placeholder(R.color.colorPrimary)
                        .error(R.color.colorPrimary)).into(imgView)
            }
        }



        /*
         pega a lista do view model e seta dentro do adapter
         é linkado diretamente no .xml do ta o recycleView
         */
        @JvmStatic
        @BindingAdapter("app:items")
        fun setItems(view: RecyclerView, list: List<Notice>?) {
            if (list != null) {
                adapter = AdapterNotice(ArrayList(list), object : AdapterNotice.ItemClickListener{
                    override fun onClick(notice: Notice) {
                        Log.i("NOTICE","listener na view")
                    }
                })
                view.adapter = adapter!!.apply {
                    notifyDataSetChanged()
                }
            }
        }


        /*
         pega a lista de paginação do view model e seta dentro do adapter
         é linkado diretamente no .xml do ta o recycleView
         */
        @JvmStatic
        @BindingAdapter("app:itemsPaginate")
        fun setItemsPaginate(view: RecyclerView, newList: List<Notice>?) {
            if (newList != null) {
                if (adapter != null){
                    adapter?.setListPaginate(newList)
                }
            }
        }



        /*
         Listener de OnTextChange -> do campo editext da busca.
         Obtém a instancia do adapter
         */
        @JvmStatic
        @BindingAdapter("app:textChanged")
        fun onTextChanged (et: EditText, count: Int) {
            et.addTextChangedListener(object : TextWatcher {
                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (adapter != null) {
                        adapter!!.filter.filter(s.toString())
                        Log.i("FILTER", s.toString())
                    }
                }
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
                override fun afterTextChanged(s: Editable) {}
            })
        }
    }


}