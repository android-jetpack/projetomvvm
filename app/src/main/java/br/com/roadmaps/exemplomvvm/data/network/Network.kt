package br.com.roadmaps.exemplomvvm.data.network
import br.com.roadmaps.pparena.data.network.CallbackNetwork


class Network : BaseNetwork() {


    private val QUERY_LIST_NOTICE: String = "http://www.json-generator.com/api/json/get/cpxOCaYRqq?indent=2"


    fun requestListNotice(cb: CallbackNetwork) {
        service(QUERY_LIST_NOTICE).get().enqueue(cb)
    }

}