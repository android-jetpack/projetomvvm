package br.com.roadmaps.exemplomvvm.data.extensions

import androidx.lifecycle.MutableLiveData
import br.com.roadmaps.exemplomvvm.data.model.ResponseNetwork
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import java.lang.Exception


fun String.toMediaJson(): RequestBody {
    val s = this.replace("'", "\"")
    val media = "application/json; charset=utf-8".toMediaTypeOrNull()
    return RequestBody.create(media, s)
}

inline fun <reified T> String.toJson(): T {
    return Gson().fromJson(this, object : TypeToken<T>() {}.type)
}

