package br.com.roadmaps.exemplomvvm.mvvm.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import br.com.roadmaps.exemplomvvm.data.model.Notice
import br.com.roadmaps.exemplomvvm.databinding.AdapterListNoticeBinding
import br.com.roadmaps.exemplomvvm.mvvm.list_notices.ListNoticesActivity
import java.util.*


class AdapterNotice (private var list: ArrayList<Notice>, private var listener: ItemClickListener): RecyclerView.Adapter< AdapterNotice.MyViewHolder>(), Filterable {


    private var listOriginal: List<Notice> = list
    private var listFiltered: List<Notice> = list
    private var mFilter: ItemFilter = ItemFilter()
    private var context: Context? = null
//    private var pos: Int = -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        context = parent.context

        val inflate = LayoutInflater.from(parent.context)
        val binding = AdapterListNoticeBinding.inflate(inflate, parent, false)
        return MyViewHolder(binding)
    }


    override fun onBindViewHolder(viewHolder: MyViewHolder, position: Int) {
        val notice = listFiltered[position]
        viewHolder.bind(notice)
    }


    override fun getItemCount(): Int = listFiltered.size


    inner class MyViewHolder(var binding: AdapterListNoticeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(notice: Notice) {
            binding.notice = notice
            binding.executePendingBindings()

//            if (pos == adapterPosition){
//                binding.layoutCell.setBackgroundColor(Color.GRAY)
//            }else{
//                binding.layoutCell.setBackgroundColor(Color.TRANSPARENT)
//            }

            binding.layoutCell.setOnClickListener {
                Log.i("CLICK",binding.notice?.title!!)
                listener.onClick(binding.notice!!)
//                binding.layoutCell.setBackgroundColor(Color.GRAY)
//                if (pos == adapterPosition){
//                    binding.layoutCell.setBackgroundColor(Color.TRANSPARENT)
//                }else{
//                    binding.layoutCell.setBackgroundColor(Color.GRAY)
//                    pos = adapterPosition
//                }
//                notifyDataSetChanged()
            }
        }
    }


    fun setListPaginate(newList: List<Notice>?){
        if (newList != null && newList.isNotEmpty()){
            list.addAll(newList)
            notifyDataSetChanged()
        }
    }


    interface ItemClickListener {
        fun onClick(notice: Notice)
    }



//================================================================================= BUSCAR ============================================================================
    override fun getFilter(): Filter {
        return mFilter
    }


    @SuppressLint("DefaultLocale")
    inner class ItemFilter : Filter() {
        override fun performFiltering(constraint: CharSequence): FilterResults {
            val filterString = constraint.toString().toLowerCase()
            val results = FilterResults()
            val list = listOriginal
            val newListNotices = ArrayList<Notice>()

            for (element in list) {
                val temp = element
                if (temp.title?.toLowerCase()?.contains(filterString)!!) {
                    newListNotices.add(temp)
                }
            }
            results.values = newListNotices
            results.count = newListNotices.size
            return results
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            listFiltered = results.values as ArrayList<Notice>
            notifyDataSetChanged()
        }
    }
}
