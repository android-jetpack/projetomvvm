package br.com.roadmaps.exemplomvvm.data.model

data class ResponseNetwork(
    var code: Int,
    var message: String?)