package br.com.roadmaps.exemplomvvm.common
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


class ViewModelProviderFactory(private val repository: Any): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return modelClass.getConstructor(repository::class.java).newInstance(repository) as T
    }
}