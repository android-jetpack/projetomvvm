package br.com.roadmaps.exemplomvvm.data.network

import br.com.roadmaps.exemplomvvm.data.extensions.toMediaJson
import br.com.roadmaps.exemplomvvm.data.model.ResponseNetwork
import br.com.roadmaps.pparena.data.network.CallbackNetwork
import com.google.gson.Gson
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import java.util.concurrent.TimeUnit
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.UnknownHostException

open class BaseNetwork {

    fun service(url: String): Request.Builder {
        return Request.Builder().url(url)
    }
    
    
    fun Request.Builder.toPost(obj: Any): Request.Builder {
        return Gson().toJson(obj).toMediaJson().let {
            this.post(it)
        }
    }

    fun Request.Builder.toPut(obj: Any): Request.Builder {
        return Gson().toJson(obj).toMediaJson().let {
            this.put(it)
        }
    }


    fun Request.Builder.toDel(obj: Any): Request.Builder {
        return Gson().toJson(obj).toMediaJson().let {
            this.delete(it)
        }
    }



    fun Request.Builder.toPostMultipart(path: String): Request.Builder {
        return fileMultipart(path).let {
            this.post(it)
        }
    }




    fun Request.Builder.enqueue(listener: () -> Callback) {
        getCall(this.build()).enqueue(listener())
    }

    fun Request.Builder.enqueue(cb: CallbackNetwork) {
        getCall(this.build()).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                when (e) {
                    is UnknownHostException -> cb.onError(ResponseNetwork(-1002, e.message))
                    else -> cb.onError(ResponseNetwork(-1001, e.message))
                }
            }

            override fun onResponse(call: Call, response: Response) {
                val responseNetwork = ResponseNetwork(response.code, response.body?.string())
                if (response.isSuccessful) {
                    cb.onSuccess(responseNetwork)
                } else {
                    try {
                        JSONObject(responseNetwork.message ?: "").let {
                            val error = it.optString("status", responseNetwork.message ?: response.message)
                            responseNetwork.message = error
                            cb.onError(responseNetwork)
                        }
                    } catch (e: JSONException) {
                        cb.onError(responseNetwork)
                    }
                }
            }
        })
    }






    private fun getCall(request: Request): Call {
        return getClient().newCall(request)
    }

    
    private fun getClient(): OkHttpClient {
        return OkHttpClient.Builder().writeTimeout(30000, TimeUnit.MILLISECONDS)
            .connectTimeout(30000, TimeUnit.MILLISECONDS).build()
    }


    private fun fileMultipart(path: String): MultipartBody {
        val requestBody = MultipartBody.Builder()
        requestBody.setType(MultipartBody.FORM)

        requestBody.addFormDataPart("image_file","image_file.jpg",
            File(path).asRequestBody("image/jpeg".toMediaType())
        )
        return requestBody.build()
    }
}


